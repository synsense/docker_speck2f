# Dockerfile for Speck2f Running Enviornment

The repository contains the dockerfile for user can locally build an image that allows X11 Forwarding(visualization), usb deployment and requirement python enviorment.

## Docker

For docker installtion on ubuntu, visit

https://docs.docker.com/engine/install/ubuntu/


## Install

With Docker installed, at the repository path:
```
sudo docker build -t synsense .
```

After build, an image should be setup and check with
```
sudo docker image ls
```

Start docker container with:

```
sudo chmod 777 ubuntu.sh
./ubuntu.sh
```

## Usage

For data recording:
```
cd /app/events_visualization_labeling-recorder
python run.py
```

For data labelling:
```
cd /app/dvs_tool-label
python run.py
```

The docker is running with deployment with 2 empty folders at your path, when use data recording tool, make sure you set the data save path to `/app/data`, The `workspace` folder is a deployment path of `your_root_path/workspace`, make sure you also put your experiment script in `/app/worksapce`. Otherwise the recorded data/your scripts **WILL NOT** be maintained once you exit the docker container.


