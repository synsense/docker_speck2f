#!/bin/bash
xhost +
sudo docker run -it \
  --rm \
  -e DISPLAY=$DISPLAY \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -v ${PWD}/apps/:/app/ \
  -v ${PWD}/apps/data/:/app/data/ \
  -v ${PWD}/apps/workspace/:/app/workspace \
  --privileged \
  -v /dev/bus/usb:/dev/bus/usb \
  synsense
