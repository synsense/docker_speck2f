from PyQt5 import QtWidgets, QtGui, QtCore
from utils import accumulate_frames
from PyQt5.QtGui import QImage, QPixmap, QKeyEvent
from mythread import MyThread
import cv2
import time
import numpy as np
import threading



class ResolutionWindow(QtWidgets.QDialog):
 
    def __init__(self,parent=None):
        super(ResolutionWindow, self).__init__(parent)
        layout = QtWidgets.QHBoxLayout()
        self.width_input = QtWidgets.QSpinBox()
        self.height_input = QtWidgets.QSpinBox()
        self.label_width = QtWidgets.QLabel()
        self.label_height = QtWidgets.QLabel()
        layout.addWidget(self.label_width)
        layout.addWidget(self.width_input)
        layout.addWidget(self.label_height)
        layout.addWidget(self.height_input)
        self.setLayout(layout)
        self.setWindowTitle("Set Resolution")
        self._init_ui()

    def _init_ui(self):
        self.label_width.setText("Height:")
        self.label_height.setText("Width:")

        self.width_input.setMaximum(10000)
        self.width_input.setMinimum(10)
        self.width_input.setValue(128)

        self.height_input.setMaximum(10000)
        self.height_input.setMinimum(10)
        self.height_input.setValue(128)


    @staticmethod
    def getResult(self,parent=None):
        dialog=ResolutionWindow(parent)
        result=dialog.exec_()
        return dialog.width_input.value(), dialog.height_input.value()
