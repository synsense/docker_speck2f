from PyQt5 import QtWidgets, QtGui, QtCore
from utils import accumulate_frames, events_to_im
from PyQt5.QtGui import QImage, QPixmap, QKeyEvent
from mythread import MyThread
from utils import draw_grids
import cv2
import time
import numpy as np
import threading



class ClipWindow(QtWidgets.QDialog):
    def __init__(self, sliced_events, w = 128, h = 128, grid_height = 1, grid_width = 1):
        super(ClipWindow, self).__init__()
        self.setWindowTitle("ClipVisualization")
        self._sliced_events = sliced_events
        self._frames_num = len(sliced_events)
        self.is_play = False
        self.width = w
        self.height = h
        self._init_ui()
        self.grid_height = grid_height
        self.grid_width = grid_width
        if self._frames_num:
            self._init_horizontal_slider()
            self.chooseFrame()

    def _init_ui(self):
        layout=QtWidgets.QVBoxLayout()

        self.graphicsView = QtWidgets.QGraphicsView()
        self.graphicsView.setGeometry(QtCore.QRect(30, 10, 521, 441))
        self.graphicsView.setObjectName("graphicsView")

        self.play_or_pause = QtWidgets.QPushButton()
        self.play_or_pause.setGeometry(QtCore.QRect(40, 520, 20, 20))
        self.play_or_pause.setObjectName("play")
        self._set_play_button(is_play=False)

        self.horizontalSlider = QtWidgets.QSlider()
        self.horizontalSlider.setGeometry(QtCore.QRect(80,520, 181, 41))
        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setObjectName("horizontalSlider")
        
        play_layout = QtWidgets.QHBoxLayout()
        play_layout.addWidget(self.play_or_pause)
        play_layout.addWidget(self.horizontalSlider)

        self.label_frame = QtWidgets.QLabel()
        self.label_frame.setGeometry(QtCore.QRect(80, 500, 67, 17))
        self.label_frame.setObjectName("label_frame")
        
        self.label_frame_total = QtWidgets.QLabel()
        self.label_frame_total.setGeometry(QtCore.QRect(550, 500, 67, 17))
        self.label_frame_total.setObjectName("label_frame_total")

        frame_num_layout = QtWidgets.QHBoxLayout()
        frame_num_layout.addWidget(self.label_frame)
        frame_num_layout.addWidget(self.label_frame_total)
    
        layout.addWidget(self.graphicsView)
        layout.addLayout(frame_num_layout)
        layout.addLayout(play_layout)
        self.setLayout(layout)

        self.setGeometry(300, 300, 600, 600)

        self._set_total_frame_num()
        self._set_connections()

    def _init_horizontal_slider(self):
        self.horizontalSlider.setValue(1)
        self.horizontalSlider.setMinimum(1)
        self.horizontalSlider.setMaximum(self._frames_num)
        self.horizontalSlider.setSingleStep(1)
        self.horizontalSlider.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.horizontalSlider.setTickInterval(max(1, int(self._frames_num/10)))


    def _set_play_button(self, is_play=True):
        if is_play:
            # self.play_or_pause.setIcon(QtGui.QIcon("img/pause.png"))
            self.play_or_pause.setStyleSheet("QPushButton{border-image: url(img/pause.png)}")
        else:
            # self.play_or_pause.setIcon(QtGui.QIcon("img/play.png"))    
            self.play_or_pause.setStyleSheet("QPushButton{border-image: url(img/play.png)}")

    def _set_connections(self):
        self.horizontalSlider.valueChanged['int'].connect(self.chooseFrame)
        self.play_or_pause.clicked.connect(self.playOrPauseFrames)   

    def _show_im(self, im, max_w_h = 420):
        if self.grid_width > 1 or self.grid_height > 1:
            im = draw_grids(im, self.grid_height, self.grid_width)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        x = im.shape[1]
        y = im.shape[0]
        self.zoomscale=max_w_h/(max(x, y))
        frame = QImage(im, x, y, QImage.Format_RGB888)
        pix = QPixmap.fromImage(frame)
        self.item=QtWidgets.QGraphicsPixmapItem(pix)
        self.item.setScale(self.zoomscale)
        self.scene=QtWidgets.QGraphicsScene()
        self.scene.addItem(self.item)
        self.graphicsView.setScene(self.scene)

    def closeEvent(self, event):
        self.is_play == False
        if isinstance(self.thread, MyThread):
            if self.thread.isRunning():
                self.thread.terminate()

    def play_frames(self):
        self._set_play_button(is_play=True)
        frame_num = self.horizontalSlider.value()
        next_frame_num = frame_num + 1
        if next_frame_num == self._frames_num:
            next_frame_num = 1
        self.horizontalSlider.setValue(next_frame_num)

    def playOrPauseFrames(self):
        if self.is_play == False:
            self.thread = MyThread()
            self.thread.signal.connect(self.play_frames)
            self.thread.start()
            self.is_play = True
        else:
            self.thread.terminate()
            self.is_play = False

    def chooseFrame(self):
        current_frame_num = self.horizontalSlider.value()
        self._set_current_frame_num(self.horizontalSlider.value())
        im = self.get_im(current_frame_num-1, self.width, self.height)
        self._show_im(im)

    def _set_current_frame_num(self, current_frame_num):
        self.label_frame.setNum(current_frame_num)

    def _set_total_frame_num(self):
        self.label_frame_total.setNum(self._frames_num)

    def get_im(self, idx, h=128, w=128):
        im = events_to_im(self._sliced_events[idx], h=h, w=w)
        return im
