
from PyQt5 import QtCore
from typing import List
import time
class MyThread(QtCore.QThread):
    signal = QtCore.pyqtSignal(str)
    def __init__(self, t_interval=30):
        super(MyThread, self).__init__()
        self.sleep_time = t_interval / 1000
        self.is_run = True

    def run(self):
        count = 0
        while True and self.is_run:
            self.signal.emit(str(count))
            time.sleep(self.sleep_time)


class WriteThread(QtCore.QThread):
     
    finish_signal = QtCore.pyqtSignal()
    def __init__(self, save_events: List, record_stop_time: int, save_file_path: str):
        super(WriteThread, self).__init__()

        self.record_tosp_time = record_stop_time
        self.save_file_path = save_file_path
        self.save_events = save_events

        self.write_start_time = None
        self.is_running = True

        self.mutex = QtCore.QMutex()
        self.once_save_num = 100

        self.stop_flag = True

    def new_events(self, events: List):
        self.mutex.lock()
        if self.is_running and events:
            self.save_events.append(events)
        self.mutex.unlock()
    
    def reset_params(self, save_events: List, record_stop_time: int, save_file_path: str):
        self.record_tosp_time = record_stop_time
        self.save_file_path = save_file_path
        self.save_events = save_events
        self.stop_flag = True

        self.write_start_time = None
        self.is_running = True


    def run(self):
        while 1:
            if self.is_running == False and len(self.save_events) == 0:
                if self.stop_flag:
                    self.stop_flag = False
                    self.save_events = []
                    self.finish_signal.emit()
            
            if self.save_events:
                once_save_events = self.save_events[:self.once_save_num]
                self.save_events = self.save_events[self.once_save_num:]

                with open(self.save_file_path, 'ab+') as f:
                    for events_list in once_save_events:
                        x, y, p, t = events_list
                        for _x, _y, _p, _t in zip(x, y, p, t):
                            f.write((_x).to_bytes(4, byteorder = 'little'))
                            f.write((_y).to_bytes(4, byteorder = 'little'))
                            f.write((_p).to_bytes(4, byteorder = 'little'))
                            f.write((_t).to_bytes(4, byteorder = 'little'))

            time.sleep(0.01)
            
            
