import copy
import numpy as np
import os
from utils import dvs_frame_to_im, read_events_from_bin, cut_events, slice_by_count, slice_by_time, accumulate_frames, events_to_im, read_events_from_aedat4

class EventsReader(object):
    def __init__(self, filename, w = 128, h = 128):
        super(EventsReader, self).__init__()
        self._load(filename)
        self._shape = (w, h)
    
    def _load(self, filename):
        self._filename = filename
        if '.bin' in filename:
            self._events = read_events_from_bin(filename)
        if '.aedat4' in filename:
            self._events = read_events_from_aedat4(filename)

    def slice_frame(self, slice_method, slice_count=1000, shape_x=128, shape_y=128):
        self._sliced_events = slice_method(self._events, slice_count)

    def set_shape(self, shape_x=240, shape_y=320):
        self._shape = (shape_x, shape_y)
        
    def get_shape(self):
        return self._shape

    def get_sliced_events(self, start_idx, end_idx, slice_method, slice_count=1000):
        events = self._events[start_idx:end_idx+1]
        sliced_events = slice_method(events, slice_count)
        return sliced_events

    def get_total_count_num(self):
        return self._events.shape[0]

    def get_total_time_window(self):
        return self._events[-1][2] - self._events[0][2]

    def get_frames_num(self):
        return len(self._sliced_events)

    def get_im(self, idx):
        im = events_to_im(self._sliced_events[idx], h=self._shape[0], w=self._shape[1]) 
        return im

    def compute_event_idx(self, frame_idx, is_end=False):
        if frame_idx == 1:
            return 0
        count = 0
        for i in range(frame_idx - 1):
            count +=  self._sliced_events[i].shape[0]
        if is_end == False:
            return count
        else:
            count += self._sliced_events[frame_idx - 1].shape[0]
        return count - 1

    def get_clip_info(self, start_frame_idx, end_frame_idx):
        start_time = self._sliced_events[start_frame_idx-1][0][2]
        end_time = self._sliced_events[end_frame_idx-1][-1][2]
        start_idx = self.compute_event_idx(start_frame_idx)
        end_idx = self.compute_event_idx(end_frame_idx, is_end=True)
        return start_time, end_time, start_idx, end_idx

    def cut_events(self, start=0, end=1):
        events = copy.deepcopy(self._events)
        return cut_events(events, start=0, end=1)

