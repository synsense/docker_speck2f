# How to use the Tool

## Data visualization and labelling
**1.Open a bin file**
click the File menu and click load, then choose a data file(*.bin)
![](./img/choose_file.png)

**2.Choose slice by time or count and set time window(spike count)**
![](./img/set_slice.png)

**3.Drag the progress bar or click the play button to view the data**
![](./img/view_data.png)

**4.View the sliced frames and set start, end and classificaiton of the segmentation clip**
![](./img/set_segmentation.png)

**5.Click insert to add info of the segmentation to the table**
![](./img/recorded_info.png)

**6.Double click the segmentation in the table to view the segmentation**
![](./img/view_segmentation.png)

**7.In the same time, it will automatically generate a csv file to record the info**
![](./img/csv_file.png) 

**6.After editting a file, just open another file, the info will save automatically**
![](./img/open_another.png) 

**7.If load a file that have been edited, the info will also be loaded**
![](./img/load_csv.png) 

**8.If insert a wrong segmentation, choose it and press delete button**
![](./img/delete.png) 

### Data Format
The data file consists of multiple DVS events that saved as a binary file(.bin). The DVS event includes 4 attributes: x , y, p, t

```
x: X-axis coordinate of the event
y: Y-axis coordinate of the event
p: Polarity of the event
t: Time-stamp of the event
```

Each attribute is stored in 4 bytes in the bin file and the byteorder is 'little'.

## Data recording
**1. Start to record**
Click the Record menu and click record, then choose a device(if there are many devices)

**2. Set storage path and filename**
![](./img/set_path_filename.png) 

**3. Start and stop recording**
![](./img/recording.png)

**4. Set Timing(optional)**
If set Timing, recording will automatically stop when the time reaches the timing.
![](./img/set_timing.png)

**5. Set CountDown(optional)**
If set CountDown, recording will start in CountDown seconds.
![](./img/set_countdown.png)

###
**Support devices:** 
1. Speck2b(Speck2btiny)
2. Speck2c-mini
3. Speck2d-mini
4. Speck2e
5. DVXPlorer-lite