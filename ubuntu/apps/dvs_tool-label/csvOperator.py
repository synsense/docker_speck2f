import csv
import pandas as pd
import os

class CsvOperator(object):
    def __init__(self, filename):
        super(CsvOperator, self).__init__()
        self.csv_file = filename

    def delete_clip_item(self, rowIndex):
        if os.path.exists(self.csv_file):
            data = pd.read_csv(self.csv_file)
            if data.shape[0] < 1:
                return
            data_new = data.drop([rowIndex])
            if data_new.shape[0] != 0:
                data_new.to_csv(self.csv_file, index=0)
            else:
                os.remove(self.csv_file)

    def add_clip_item(self, start_time, end_time, start_idx, end_idx, classification):
        if not os.path.exists(self.csv_file):
            with open(self.csv_file, mode='w', newline='', encoding='utf8') as cf:
                wf=csv.writer(cf)
                wf.writerow(['startTime', 'endTime', 'startIdx', 'endIdx', 'classification'])
                data = [str(start_time), str(end_time), str(start_idx), str(end_idx), str(classification)]
                wf.writerow(data)
        else:
            with open(self.csv_file, mode='a', newline='', encoding='utf8') as cfa:
                wf = csv.writer(cfa)
                data = [str(start_time), str(end_time), str(start_idx), str(end_idx), str(classification)]
                wf.writerow(data)

    def load_clip_items(self):
        if not os.path.exists(self.csv_file):
            return []
        rows = []
        with open(self.csv_file, 'r') as f:
            reader = csv.reader(f)
            for idx, row in enumerate(list(reader)):
                if idx != 0:
                    row = list(map(int, row))
                    rows.append(row)
        return rows      