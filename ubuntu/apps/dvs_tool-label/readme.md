# DVS Tool
This tool is used to dvs data's visualization, labelling, and recording.


## Quick Start
***

**Environment**
````
python >= 3.6
Ubuntu >= 18.04
````

**Install**


1. Clone this project
````
git clone git@spinystellate.office.synsense.ai:chengduae/events_visualization_labeling.git
````

2. Install dependencies
````
pip install -r requirements.txt
````

### How to use

````
python run.py
````

Specific instructions please see [UserGuide.md](./userGuide.md). You can try with the example data(./data/attentive_driving_libo_001.bin)

**Keyboard Shortcuts**
````
S: Set current frame as Start.
E: Set current frame as End.
I: Insert current clip(from Start to End) to the Table.
````
### P.S.
Part of the code(slice_by_time, slice_by_count, accumulate_frames, make_structured_array in utils.py) is quoted from [aermanager](https://spinystellate.office.synsense.ai/research/SCNN/aermanager).
Consideration of the difficulty of installation and the generated executable file is too large, this code is not import aermanager and just copy code of the 4 functions.

## Todo
- [x] Add video play function.
- [x] Add data recording function.
- [x] Support data of different resolution.
- [x] Visualization of the segmentations.
- [ ] Use tonic to parse and slice events instead of using the code of aermanager.
- [ ] Support other filetypes like aedat4, dvs_ibm and so on.
- [ ] Adaptive window size.
- [ ] Add code annotations.