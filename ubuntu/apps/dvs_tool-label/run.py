# -*- coding: utf-8 -*-
 
from PyQt5 import QtWidgets, QtGui
from utils import slice_by_time, slice_by_count, draw_grids
from eventsReader import EventsReader
from csvOperator import CsvOperator
from clipwindow import ClipWindow
from mythread import MyThread
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QTableWidgetItem
from utils import load_filedir, save_filename_to_file
from resolutionwindow import ResolutionWindow
from gridwindow import GridWindow
import os
import sys
import cv2
from ui import Ui_MainWindow     
 
class mywindow(QtWidgets.QMainWindow,Ui_MainWindow):    
    def __init__(self):    
        super(mywindow,self).__init__()    
        self.setupUi(self)


        self._init_params()


    def _init_params(self):
        self.is_play = False
        self._class = 0
        self._clip_start = 1
        self._clip_end = 1

        self._slice_count = 1000
        self._slice_time = 1000
        self._is_slice_time = False
        self._is_slice_count = True

        self.width = 128
        self.height = 128

        self.grid_width = 1
        self.grid_height = 1

    def _init_slice_widgets(self):
        self.SliceSpikeCount.setMinimum(100)
        self.SliceSpikeCount.setMaximum(self.events_reader.get_total_count_num())
        self.SliceSpikeCount.setValue(self._slice_count)

        self.SliceTimeWindow.setMinimum(100)
        self.SliceTimeWindow.setMaximum(self.events_reader.get_total_time_window())
        self.SliceTimeWindow.setValue(self._slice_time)

        if not self.isSliceCount.isChecked() and not self.isSliceTime.isChecked():
            self.isSliceCount.setChecked(True)


    def _init_table_widget(self):
        self.TableWidget.setRowCount(0)
        self.TableWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.TableWidget.setColumnCount(5)
        self.TableWidget.setHorizontalHeaderLabels(['startTime','endTime','startIdx', 'endIdx', 'class'])

        self._load_clips_items_from_file()

    def _load_clips_items_from_file(self):
        for row in self.csv_operator.load_clip_items():
            start_time, end_time, start_idx, end_idx, classification = row
            self._add_clip_item_to_table(start_time, end_time, start_idx, end_idx, classification, add_to_file=False)

    def _add_clip_item_to_table(self, start_time, end_time, start_idx, end_idx, classification, add_to_file=True):
        if not hasattr(self, 'events_reader'):
            return

        row_num = self.TableWidget.rowCount()
        self.TableWidget.setEditTriggers(QtWidgets.QAbstractItemView.CurrentChanged)
        self.TableWidget.setRowCount(row_num+1)
        self.TableWidget.setItem(row_num, 0, QTableWidgetItem(str(start_time)))
        self.TableWidget.setItem(row_num, 1, QTableWidgetItem(str(end_time)))
        self.TableWidget.setItem(row_num, 2, QTableWidgetItem(str(start_idx)))
        self.TableWidget.setItem(row_num, 3, QTableWidgetItem(str(end_idx)))
        self.TableWidget.setItem(row_num, 4, QTableWidgetItem(str(classification)))
        self.TableWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        if add_to_file:
            self._add_clip_item_to_file(start_time, end_time, start_idx, end_idx, classification)

    def _add_clip_item_to_file(self, start_time, end_time, start_idx, end_idx, classification):
        csv_file = self.filename.replace('.bin', '.csv')
        self.csv_operator.add_clip_item(start_time, end_time, start_idx, end_idx, classification) 

    def _reset_params(self):
        self._class = 0
        self._clip_start = 0
        self._clip_end = 1
    
    def _reset_slice(self):
        if self.isSliceCount.isChecked():
            slice_method = slice_by_count
            slice_count = self._slice_count
        else:
            slice_method = slice_by_time
            slice_count = self._slice_time
        self.events_reader.slice_frame(slice_method, slice_count)
        self._frames_num = self.events_reader.get_frames_num()
        self._set_total_frame_num(self._frames_num)
        self._reset_frame_slide()
        self._reset_clip_start()
        self._reset_clip_end()

    def _reset_frame_slide(self):
        self.horizontalSlider.setValue(1)
        self.horizontalSlider.setMinimum(1)
        self.horizontalSlider.setMaximum(self._frames_num)
        self.horizontalSlider.setSingleStep(1)
        self.horizontalSlider.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.horizontalSlider.setTickInterval(max(2, int(self._frames_num/10)))

        self.chooseFrame()
    
    def _reset_clip_start(self):
        self.clip_start.setMaximum(self._frames_num)
        self.clip_start.setMinimum(1)
        self.clip_start.setValue(1)
    
    def _reset_clip_end(self):
        self.clip_end.setMaximum(self._frames_num)
        self.clip_end.setMinimum(1)
        self.clip_end.setValue(self._frames_num)    

    def _show_im(self, im, max_w_h = 400):
        if self.grid_width > 1 or self.grid_height > 1:
            im = draw_grids(im, self.grid_height, self.grid_width)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        x = im.shape[1]
        y = im.shape[0]
        self.zoomscale=max_w_h/(max(x, y))
        frame = QImage(im, x, y, QImage.Format_RGB888)
        pix = QPixmap.fromImage(frame)
        self.item=QtWidgets.QGraphicsPixmapItem(pix)
        self.item.setScale(self.zoomscale)
        self.scene=QtWidgets.QGraphicsScene()
        self.scene.addItem(self.item)
        self.graphicsView.setScene(self.scene)                    

    def _set_current_frame_num(self, current_frame_num):
        self.label_frame.setNum(current_frame_num)

    def _set_total_frame_num(self, total_frame_num):
        self.label_frame_total.setNum(total_frame_num)

    def _delete_clip_item_from_file(self, rowIndex):
        self.csv_operator.delete_clip_item(rowIndex)

    def loadBinFile(self):
        filedir = load_filedir('record.txt')
        fname, _  = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file', filedir, "Bin files (*.bin);; Aedat4(*.aedat4)")
        _, suffix = os.path.splitext(fname)
        self.filename = fname
        save_filename_to_file(self.filename)
        self.events_reader = EventsReader(fname, w=self.width, h=self.height)
        # self.events_reader.set_shape(240, 320)
        self.csv_operator = CsvOperator(self.filename.replace(suffix, '.csv'))
        self.label_filename.setText(self.filename)
        self._init_slice_widgets()
        self._reset_slice()
        self._init_table_widget()

    def playOrPauseFrames(self):
        if hasattr(self, 'events_reader'):
            if self.is_play == False:
                self.is_play = True
                self.thread = MyThread()
                self.thread.signal.connect(self.play_frames)
                self.thread.start()
                self.set_play_button(is_play=True)
                self.isSliceCount.setEnabled(False)
                self.isSliceTime.setEnabled(False)
                self.SliceTimeWindow.setEnabled(False)
                self.SliceSpikeCount.setEnabled(False)
            else:
                self.thread.terminate()
                self.set_play_button(is_play=False)
                self.isSliceCount.setEnabled(True)
                self.isSliceTime.setEnabled(True)
                self.SliceTimeWindow.setEnabled(True)
                self.SliceSpikeCount.setEnabled(True)                
                self.is_play = False
    
    def play_frames(self):
        frame_num = self.horizontalSlider.value()
        next_frame_num = frame_num + 1
        if next_frame_num == self._frames_num:
            next_frame_num = 1
        self.horizontalSlider.setValue(next_frame_num)

    def setSliceTimeWindow(self):
        self.isSliceTime.setChecked(True)
        self._slice_time = self.SliceTimeWindow.value()
        if hasattr(self, 'events_reader'):
            self._reset_slice()

    def setSliceSpikeCount(self):
        self.isSliceCount.setChecked(True)
        self._slice_count = self.SliceSpikeCount.value()
        if hasattr(self, 'events_reader'):
            self._reset_slice()

    def chooseFrame(self):
        if hasattr(self, 'events_reader'):
            current_frame_num = self.horizontalSlider.value()
            self._set_current_frame_num(self.horizontalSlider.value())
            im = self.events_reader.get_im(current_frame_num-1)
            self._show_im(im)

    def chooseSliceTime(self):
        if hasattr(self, 'events_reader'):
            self._reset_slice()

    def chooseSliceSpike(self):
        if hasattr(self, 'events_reader'):
            self._reset_slice()

    def insertClipItem(self):
        if not hasattr(self, 'events_reader'):
            return
        if not self.clip_start.value() < self.clip_end.value():
            QtWidgets.QMessageBox.about(self, 'invalid', "start must be less than end")
            return
        start_time, end_time, start_idx, end_idx = self.events_reader.get_clip_info(self.clip_start.value(), self.clip_end.value())
        classificaiton = self.classification.value()
        self._add_clip_item_to_table(start_time, end_time, start_idx, end_idx, classificaiton)

    def deleteClipItem(self):
        rowIndex = self.TableWidget.currentRow()
        if rowIndex != -1:
            self.TableWidget.removeRow(rowIndex)
            self._delete_clip_item_from_file(rowIndex)

    def keyPressEvent(self, QkeyEvent):
        if QkeyEvent.key() == 65:
            self.horizontalSlider.setValue(max(1, self.horizontalSlider.value()-1))
        if QkeyEvent.key() == 68:
            self.horizontalSlider.setValue(min(self._frames_num, self.horizontalSlider.value()+1))        
        if QkeyEvent.key() == 32:
            self.playOrPauseFrames()
        if QkeyEvent.key() == 83:
            self.clip_start.setValue(self.horizontalSlider.value())
        if QkeyEvent.key() == 69:
            self.clip_end.setValue(self.horizontalSlider.value())
        if QkeyEvent.key() == 73:
            self.insertClipItem()
        if QkeyEvent.key() in list(range(48, 58)):
            num = QkeyEvent.key() - 48
            self.classification.setValue(num)

    def showClip(self):
        if self.isSliceCount.isChecked():
            slice_method = slice_by_count
            slice_count = self._slice_count
        else:
            slice_method = slice_by_time
            slice_count = self._slice_time
        items = self.TableWidget.selectedItems()
        start_idx = int(items[2].text())
        end_idx = int(items[3].text())
        sliced_events = self.events_reader.get_sliced_events(start_idx, end_idx, slice_method, slice_count)
        child_window = ClipWindow(sliced_events=sliced_events, w=self.width, h=self.height, grid_height = self.grid_height, grid_width = self.grid_width)
        child_window.exec()       

    def setResolution(self):
        self.width, self.height = ResolutionWindow.getResult(self)
        if hasattr(self, 'events_reader'):
            self.events_reader.set_shape(self.width, self.height)
            current_frame_num = self.horizontalSlider.value()
            if current_frame_num > 1:
                self.horizontalSlider.setValue(current_frame_num-1)
            else:
                self.horizontalSlider.setValue(current_frame_num+1)
            self.horizontalSlider.setValue(current_frame_num)

    def setViewgrid(self):
        self.grid_width, self.grid_height = GridWindow.getResult(self)
        if hasattr(self, 'events_reader'):
            current_frame_num = self.horizontalSlider.value()
            if current_frame_num > 1:
                self.horizontalSlider.setValue(current_frame_num-1)
            else:
                self.horizontalSlider.setValue(current_frame_num+1)
            self.horizontalSlider.setValue(current_frame_num)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = mywindow()
    window.show()
    sys.exit(app.exec_())
