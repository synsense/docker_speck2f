# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'record.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!
import time
import numpy as np
import os
import json
from PyQt5 import QtCore, QtGui, QtWidgets
from utils import next_filename
from PyQt5.Qt import QLCDNumber
from mythread import MyThread, WriteThread, ReadThread, ClockThread

class RecordWindow(QtWidgets.QDialog):


    record_stop_signal = QtCore.pyqtSignal()
    write_events_signal = QtCore.pyqtSignal()
    def __init__(self, dk=None, buf=[], device_type='Speck2b', grid_height = 1, grid_width = 1, visualizer=None):
        super(RecordWindow, self).__init__()

        self.graphicsView = QtWidgets.QGraphicsView()
        self.graphicsView.setGeometry(QtCore.QRect(20, 40, 250, 250))
        self.graphicsView.setObjectName("graphicsView")
        self.pushButton_3 = QtWidgets.QPushButton()
        self.pushButton_3.setGeometry(QtCore.QRect(360, 110, 89, 25))
        self.pushButton_3.setObjectName("pushButton_3")

        self.horizontalLayoutWidget = QtWidgets.QWidget()
        #self.horizontalLayoutWidget.setGeometry(QtCore.QRect(60, 240, 181, 61))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")

        self.horizontalLayoutWidget_2 = QtWidgets.QWidget()
        #self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(60, 300, 181, 160))
        self.horizontalLayoutWidget_2.setObjectName("horizontalLayoutWidget_2")

        self.horizontalLayoutWidget_3 = QtWidgets.QWidget()
        #self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(60, 300, 181, 160))
        self.horizontalLayoutWidget_3.setObjectName("horizontalLayoutWidget_3")

        self.horizontalLayoutWidget_4 = QtWidgets.QWidget()
        #self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(60, 300, 181, 160))
        self.horizontalLayoutWidget_4.setObjectName("horizontalLayoutWidget_4")

        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.playButton = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.playButton.setObjectName("playButton")
        self.horizontalLayout.addWidget(self.playButton)
    
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.lcdNumber = QLCDNumber(12, self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.addWidget(self.lcdNumber)


        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.setTimingButton = QtWidgets.QRadioButton(self.horizontalLayoutWidget_3)
        self.timingSpinbox = QtWidgets.QSpinBox(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.addWidget(self.setTimingButton)
        self.horizontalLayout_3.addWidget(self.timingSpinbox)

        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_4)
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.setCountDownButton = QtWidgets.QRadioButton(self.horizontalLayoutWidget_4)
        self.countDownSpinbox = QtWidgets.QSpinBox(self.horizontalLayoutWidget_4)
        self.horizontalLayout_4.addWidget(self.setCountDownButton)
        self.horizontalLayout_4.addWidget(self.countDownSpinbox)

        #self.fileButton = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        # self.fileButton.setObjectName("fileButton")
        # self.horizontalLayout.addWidget(self.fileButton)
        self.gridLayoutWidget = QtWidgets.QWidget()
        self.gridLayoutWidget.setGeometry(QtCore.QRect(300, 150, 221, 80))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.label_3 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.lineEdit.setText("")
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 2, 1, 1, 1)
        self.label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)

        layout = QtWidgets.QVBoxLayout()

        filename_layout=QtWidgets.QVBoxLayout()
        filename_layout.addWidget(self.pushButton_3)
        filename_layout.addWidget(self.gridLayoutWidget)


        layout.addWidget(self.graphicsView)
        layout.addWidget(self.horizontalLayoutWidget_2)
        layout.addWidget(self.horizontalLayoutWidget_3)
        layout.addWidget(self.horizontalLayoutWidget_4)
        layout.addWidget(self.horizontalLayoutWidget)
        layout.addLayout(filename_layout)
        #layout.addLayout(record_layout)
        #layout.addLayout(filename_layout)
        self.setLayout(layout)
        self.setGeometry(300, 300, 600, 600)

        self.retranslateUi()
        self.playButton.clicked.connect(self.record_or_pause)
        # self.fileButton.clicked.connect(self.stop_record)
        self.pushButton_3.clicked.connect(self.choose_directory)
        # self.lineEdit.textEdited['QString'].connect(self.choose_directory)

        self.max_read_time = -1
        self.max_to_im_time = -1
        self.max_show_im_time = -1
        self.dk = dk
        self.buf = buf
        self.device_type = device_type
        self.buf.get_events()
        self.save_events = []
        self.is_recording = False
        self.is_reading = False
        self.is_show = False
        self.start_read_buf()
        self._init_widget()
        self.load_settings()
        self._init_resolution()
        self.grid_height = grid_height
        self.grid_width = grid_width

        self.time_mutex = QtCore.QMutex()

        self.visualizer = visualizer
        # 创建定时器并设置超时连接
        self.check_visualizer_timer = QtCore.QTimer(self)
        self.check_visualizer_timer.timeout.connect(self.check_visualizer)
        self.check_visualizer_timer.start(100)         

        self.setStyleSheet("#MainWindow{background-color:green}")

    def check_visualizer(self):
        if not self.visualizer.is_open():
            self.close()

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("", ""))
        self.pushButton_3.setText(_translate("", "choose"))
        self.playButton.setText(_translate("", "Record"))
        self.setTimingButton.setText(_translate("", "Timing(s)"))
        self.setCountDownButton.setText(_translate("", "CountDown(s)"))
        # self.fileButton.setText(_translate("", "Stop"))
        self.label_3.setText(_translate("", "Filename:"))
        self.label.setText(_translate("", "TextLabel"))
        self.label_2.setText(_translate("", "Directory:"))

    def _init_widget(self):
        self.label.setText('/home')
        self.lcdNumber.display('00:00:00.000')
        self.lcdNumber.setSegmentStyle(QLCDNumber.Flat)
        self.timingSpinbox.setMaximum(86400*99)

    def _init_resolution(self):
        resolution_dict = {'Speck2b':(128, 128), 'Speck2c':(64, 64), 'DVXplorer':{240, 320}, 'Speck2d':(64, 64), 'Speck2e':(128, 128), 'Speck2et':(128, 128), 'Speck2f':(128, 128)}
        self.h, self.w = resolution_dict[self.device_type]

    def save_settings(self):
        file_dir = self.label.text()
        is_timing = self.setTimingButton.isChecked()
        timming = self.timingSpinbox.value()
        is_countdown = self.setCountDownButton.isChecked()
        countdown = self.countDownSpinbox.value()
        filename = self.lineEdit.text()
        config = {'file_dir': file_dir, 'is_timing': is_timing, 'timing': timming, 'is_countdown': is_countdown, 
                'filename': filename, 'countdown': countdown}

        with open('config.json', 'w') as f:
            json.dump(config, f)

    def load_settings(self):
        try:
            with open('config.json', 'r') as f:
                config = json.load(f)
                file_dir = config['file_dir']
                is_timing = config['is_timing']
                timing = config['timing']
                is_countdown = config['is_countdown']
                countdown = config['countdown']
                filename = config['filename']
                if os.path.exists(file_dir):
                    self.file_dir = file_dir
                    self.label.setText(self.file_dir)
                else:
                    raise FileNotFoundError

                self.lineEdit.setText(filename)
                if is_timing:
                    self.setTimingButton.setChecked(True)
                if is_countdown:
                    self.setCountDownButton.setChecked(True)
                self.timingSpinbox.setValue(timing)
                self.countDownSpinbox.setValue(countdown)
        except:
            self.file_dir = '/home'
            self.label.setText(self.file_dir)
            self.setTimingButton.setChecked(False)
            self.setCountDownButton.setChecked(False)
            self.timingSpinbox.setValue(0)
            self.countDownSpinbox.setValue(0)
        else:
            pass 

    def closeEvent(self, event):
        self.is_recording = False
        self.is_reading = False
        self.is_show = False
        self.save_settings()
        if hasattr(self, 'thread_1') and isinstance(self.thread_1, MyThread):
            self.thread_1.is_run = False
            if self.thread_1.isRunning():
                self.thread_1.terminate()
        if hasattr(self, 'thread_2') and isinstance(self.thread_2, MyThread):
            self.thread_2.is_run = False
            if self.thread_2.isRunning():
                self.thread_2.terminate()
        if hasattr(self, 'thread_write') and isinstance(self.thread_write, WriteThread):
            self.thread_write.is_run = False
            if self.thread_write.isRunning():
                self.thread_write.terminate()

    def start_read_buf(self):
        self.thread_read = ReadThread(buf = self.buf, is_recording = False)
        self.thread_read.start()

    def record_or_pause(self):
        if self.is_recording == True:
            self.is_recording = False
            self.thread_clock.is_run = False
            self.stop_record()
        else:
            filename = self.lineEdit.text()
            if len(filename) == 0:
                QtWidgets.QMessageBox.warning(self, "Warning", "Please input filename")
                return

            if self.setCountDownButton.isChecked() and self.countDownSpinbox.value() > 0:
                self.wait_time = self.countDownSpinbox.value()
                self.thread_5 = MyThread(1000)
                self.thread_5.signal.connect(self.update_wait_time)
                self.thread_5.start()
                return

            self.start_record()

    def start_record(self):
        filename = self.lineEdit.text()
        self.save_filename = os.path.join(self.label.text(), filename+'.bin')
        if os.path.exists(self.save_filename):
            os.remove(self.save_filename)

        self.save_events = []
        self.is_recording = True
        stopWatch = self.dk.get_stop_watch()
        stopWatch.set_enable_value(True)
        stopWatch.reset()
        time.sleep(0.1)           
        self.playButton.setText('stop')

        self.record_time = 0
        if self.setTimingButton.isChecked() and self.timingSpinbox.value() > 0:
            self.record_time = self.timingSpinbox.value()
            self.record_start_time = time.time()
            self.record_stop_time = self.record_start_time + self.record_time
        else:
            self.record_time = 10000
            self.record_start_time = time.time()
            self.record_stop_time = time.time() + 10000
        
        self.thread_read.end_time = self.record_stop_time
        self.thread_read.is_recording = True
        if not hasattr(self, 'thread_write'):
            self.thread_write = WriteThread(save_events = self.save_events, record_stop_time = self.record_stop_time, save_file_path = self.save_filename)
            # self.write_events_signal.connect(lambda:self.thread_write.new_events(self.save_events))
            self.thread_read.send_events_signal.connect(lambda:self.thread_write.new_events(self.thread_read.get_events()))
            self.thread_write.finish_signal.connect(self.finish_record) 
            self.thread_write.start()
        else:
            self.thread_write.reset_params(save_events = self.save_events, record_stop_time = self.record_stop_time, save_file_path = self.save_filename)

        if not hasattr(self, 'thread_clock'):
            self.thread_clock = ClockThread(record_time=self.record_time)
            self.thread_clock.update_signal.connect(lambda:self.update_time(self.thread_clock.get_time_str()))
            self.thread_clock.time_finish_signal.connect(self.time_finish_stop)
            self.thread_clock.start()
        else:
            self.thread_clock.reset(record_time=self.record_time)
            

    def update_wait_time(self):
        self.playButton.setText(f'wait {self.wait_time}s')
        self.wait_time -= 1
        print(self.wait_time)
        if self.wait_time < 0:
            self.thread_5.is_run = False
            self.start_record()

    def stop_record(self):
        self.thread_read.is_recording = False
        self.thread_write.is_running = False
        if hasattr(self, 'thread_4'):
            self.thread_4.is_run = False
        self.playButton.setText('Please wait, writing to file...')
        self.playButton.enabled = False
        self.lineEdit.setText(next_filename(self.lineEdit.text()))

    def finish_record(self):
        self.playButton.enabled = True
        self.playButton.setText('record')
        self.save_events = []

    def time_finish_stop(self):
        self.is_recording = False
        self.stop_record()

    def update_time(self, time_str):
        self.lcdNumber.display(time_str)

    def choose_directory(self):
        dir_path = QtWidgets.QFileDialog.getExistingDirectory(self, 'open directory', self.file_dir)
        self.file_dir = dir_path
        self.label.setText(dir_path)

