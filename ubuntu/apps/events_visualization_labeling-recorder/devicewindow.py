from PyQt5 import QtWidgets, QtGui, QtCore



class DeviceWindow(QtWidgets.QDialog):
 
    def __init__(self, devices=[], parent=None):
        super(DeviceWindow, self).__init__()
        layout = QtWidgets.QHBoxLayout()
        self.devices_box = QtWidgets.QComboBox()
        self._init_devices_box(devices)
        layout.addWidget(self.devices_box)
        self.setLayout(layout)
        self.setWindowTitle("choose device")


    def _init_devices_box(self, devices=[]):
        for device in devices:
            self.devices_box.addItem(f'Bus {device.usb_bus_number} Device {device.usb_device_address} {device.device_type_name}')


    @staticmethod
    def getResult(parent=None, devices=[]):
        dialog=DeviceWindow(devices, parent)
        result=dialog.exec_()
        return dialog.devices_box.currentIndex()
