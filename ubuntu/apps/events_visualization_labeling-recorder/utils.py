import os
import numpy as np
import socket
import samna
from typing import Tuple, List

def find_free_port():
    # 创建一个socket对象
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # 将socket绑定到一个临时端口
    s.bind(('localhost', 0))

    # 获取绑定的端口号
    port = s.getsockname()[1]

    # 关闭socket
    s.close()

    return port

def get_device_list():
    """Get a list of unopened devices.

    Returns:
        A list containing all unopened devices.
    """
    return samna.device.get_unopened_devices()


def parse_events(events):
    """Convert a list of events into a list of tuples.

    Args:
        events: A list of Event objects, each containing x, y, feature, and timestamp attributes.

    Returns:
        A list of tuples, where each tuple contains the x, y, feature, and timestamp attributes of an event.
    """
    event_tuples = [(event.x, event.y, event.feature, event.timestamp) for event in events]
    return event_tuples


def load_filedir(record_filename, filedir = '/home'):
    if os.path.exists(record_filename):
        with open(record_filename, 'r') as f:
            lines = f.readlines()
            if len(lines) > 0:
                filename = lines[0].strip()
                filedir, _ = os.path.split(filename)
    return filedir

def save_filename_to_file(filename, record_filename='record.txt'):
    with open(record_filename, 'w') as f:
        f.write(filename+'\n')    

def next_filename(filename):
    if '_' not in filename:
        return filename
    splits = filename.split('_')
    try:
        num_str = splits[-1]
        num = int(num_str)
    except:
        return filename
    num += 1
    len_num = len(num_str)
    num_str = str(num).zfill(len_num)
    splits[-1] = num_str
    filename = '_'.join(splits)
    return filename

def get_dis(pt1, pt2):
    import math
    dis = math.sqrt((pt2[0]-pt1[0])**2+(pt2[1]-pt1[1])**2)
    return dis

if __name__ == "__main__":
    pass
