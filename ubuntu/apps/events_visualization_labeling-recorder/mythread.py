
from PyQt5 import QtCore
from typing import List
from utils import parse_events
import time

class MyThread(QtCore.QThread):
    signal = QtCore.pyqtSignal(str)
    def __init__(self, t_interval=30):
        super(MyThread, self).__init__()
        self.sleep_time = t_interval / 1000
        self.is_run = True

    def run(self):
        count = 0
        while True and self.is_run:
            self.signal.emit(str(count))
            time.sleep(self.sleep_time)

class ReadThread(QtCore.QThread):
     
    send_events_signal = QtCore.pyqtSignal()
    def __init__(self, buf: List, is_recording: False, end_time = -1):
        super(ReadThread, self).__init__()

        self.buf = buf
        self.is_recording = is_recording
        self.save_events = []
        self.end_time = end_time
        self.mutex = QtCore.QMutex()



    def get_events(self):
        self.mutex.lock()
        events = self.save_events
        self.save_events = []
        self.mutex.unlock()
        return events
    
    def run(self):
        while 1:
            if self.is_recording == False:
                self.buf.get_events()
                time.sleep(0.01)
                continue

            start = time.time()
            events = self.buf.get_events()
            end = time.time()

            if end < start:
                time.sleep(0.01)
                continue

            events = parse_events(events)
            self.save_events = events
            self.send_events_signal.emit()
            time.sleep(0.01)
            continue          

class ClockThread(QtCore.QThread):
    update_signal = QtCore.pyqtSignal()
    time_finish_signal = QtCore.pyqtSignal()
    def __init__(self, record_time = 999999999999):
        super(ClockThread, self).__init__()
        self.start_time = time.time()
        self.end_time = self.start_time + record_time
        self.is_run = True

    def reset(self, record_time = 999999999999):
        self.start_time = time.time()
        self.end_time = self.start_time + record_time
        self.is_run = True
        self.time_str = str(0).zfill(2)+':'+str(0).zfill(2)+':'+str(0).zfill(2)+'.'+str(0).zfill(3)

    def get_time_str(self):
        return self.time_str

    def run(self):
        while 1:
            if self.is_run:
                time_now = min(time.time(), self.end_time)
                if time_now < self.end_time:
                    time_count = int((time_now - self.start_time) * 1000)
                    s, ms = divmod(time_count, 1000)
                    m, s = divmod(s, 60)
                    h, m = divmod(m, 60)
                    self.time_str = str(h).zfill(2)+':'+str(m).zfill(2)+':'+str(s).zfill(2)+'.'+str(ms).zfill(3)
                    self.update_signal.emit()
                else:
                    self.is_run = False
                    time_count = int((self.end_time - self.start_time) * 1000)
                    s, ms = divmod(time_count, 1000)
                    m, s = divmod(s, 60)
                    h, m = divmod(m, 60)
                    self.time_str = str(h).zfill(2)+':'+str(m).zfill(2)+':'+str(s).zfill(2)+'.'+str(ms).zfill(3)
                    self.update_signal.emit()
                    self.time_finish_signal.emit()                    
            else:
                pass

            time.sleep(0.001)


class WriteThread(QtCore.QThread):
     
    finish_signal = QtCore.pyqtSignal()
    def __init__(self, save_events: List, record_stop_time: int, save_file_path: str):
        super(WriteThread, self).__init__()

        self.record_tosp_time = record_stop_time
        self.save_file_path = save_file_path
        self.save_events = save_events

        self.write_start_time = None
        self.is_running = True

        self.mutex = QtCore.QMutex()
        self.once_save_num = 10000

        self.stop_flag = True

    def new_events(self, events: List):
        self.mutex.lock()
        if self.is_running and events:
            self.save_events += events
        self.mutex.unlock()
    
    def reset_params(self, save_events: List, record_stop_time: int, save_file_path: str):
        self.record_tosp_time = record_stop_time
        self.save_file_path = save_file_path
        self.save_events = save_events
        self.stop_flag = True

        self.write_start_time = None
        self.is_running = True


    def run(self):
        while 1:
            if self.is_running == False and len(self.save_events) == 0:
                if self.stop_flag:
                    self.stop_flag = False
                    self.save_events = []
                    self.finish_signal.emit()
            
            if self.save_events:
                once_save_events = self.save_events[:self.once_save_num]
                self.save_events = self.save_events[self.once_save_num:]

                with open(self.save_file_path, 'ab+') as f:
                    for event in once_save_events:
                        x, y, p, t = event
                        f.write((x).to_bytes(4, byteorder = 'little'))
                        f.write((y).to_bytes(4, byteorder = 'little'))
                        f.write((p).to_bytes(4, byteorder = 'little'))
                        f.write((t).to_bytes(4, byteorder = 'little'))

            time.sleep(0.01)
            
            
