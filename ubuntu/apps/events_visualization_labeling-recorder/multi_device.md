# DVS录制工具 DVS recorder使用教程

## 环境要求

````
python >= 3.6
ubuntu 18.04及以上(推荐20.04)
````

## 安装

````
pip install -r requirements.txt
````

## 运行

````
python3 run.py
````

## 使用方法
使用方法和老版<DVS tool>一致，参考其说明文档即可。

## 多设备录制

1. PC连接多台设备(确保是USB3.0接口)
2. 打开一个终端, 运行DVS recorder

````
python3 run.py
````
3. 弹出一个窗口选择设备，选择好后弹出DVS画面和控制界面。
4. 重新打开一个终端，运行DVS recorder，选择另一设备
5. 打开所有设备后，依次设置，点击录制即可。