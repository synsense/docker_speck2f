# -*- coding: utf-8 -*-
 

import sys


import numpy as np
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets
import signal

from PyQt5.QtCore import QTimer
from device_factory import DeviceFactory
from devicewindow import DeviceWindow
from record_window import RecordWindow
from utils import get_device_list

def choose_device() -> str:
    """
    This function displays a device selection window and returns the selected device name.
    Returns:
        str: The name of the selected device.
    """
    devices = get_device_list()
    if len(devices) == 1:
        device_num = 0
    elif len(devices) > 1:
        device_num = DeviceWindow.getResult(None, devices)
    else:
        raise Exception("No devices were found.")

    device_name = devices[device_num].device_type_name
    print(device_name)
    return device_name


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    signal.signal(signal.SIGINT, lambda *args: app.quit())

    device_name = choose_device()
    device = DeviceFactory.create_device(device_name)

    # Start a DVS show and write data to a buffer
    dk, visualizer, buf, gui_thread, dvs_graph = device.start_dvs_show_and_to_buf()

    record_window = RecordWindow(dk=dk, buf=buf, visualizer=visualizer)
    record_window.show()

    timer = QTimer()
    timer.timeout.connect(lambda: None)
    timer.start(500)

    # Start the Qt event loop
    sys.exit(app.exec_())
