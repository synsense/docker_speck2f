from abc import ABC, abstractmethod
from threading import Thread
from utils import find_free_port
import os
import sys
import time
import samna.graph
import samna.device

class Device(ABC):
    """
    Abstract base class for devices
    """

    def __init__(self, name, width, height, dvs_converter_name):
        self.name = name
        self.width = width
        self.height = height
        self.dvs_conveter_name = dvs_converter_name
        self.port = find_free_port()
    
    @abstractmethod
    def init_buf(self):
        """
        Initialize the buffer to save DVS events
        """

    @abstractmethod
    def apply_device(self):
         """
         Applies the device to the given data
         """

    def get_device_name(self):
        return self.name

    def open_device(self):
        self.dk = samna.device.open_device(self.name)

    def build_samna_route(self):
        """
        Builds a graph in samna to show DVS events
        """
        self.dvs_graph = samna.graph.EventFilterGraph()
        
        # Connect the model source node and the buffer node in sequence
        _, _ = self.dvs_graph.sequential([self.dk.get_model_source_node(), self.buf])
        
        # Connect the model source node, DVS converter node and VizEventStreamer node in sequence
        _, _, streamer = self.dvs_graph.sequential([self.dk.get_model_source_node(), self.dvs_conveter_name, "VizEventStreamer"])

        # Set the endpoint of the VizEventStreamer node
        streamer.set_streamer_endpoint(f"tcp://0.0.0.0:{self.port}")

    def build_samnagui_event_route(self, plot_id):
        """
        Sets the Viz events route in samnagui
        """
        # Set the receiver endpoint of the visualizer

        self.visualizer.receiver.set_receiver_endpoint(f"tcp://0.0.0.0:{self.port}")
        
        # Add the visualizer splitter as the destination of the receiver
        self.visualizer.receiver.add_destination(self.visualizer.splitter.get_input_channel())
        
        # Add the specified plot as the destination of the splitter
        self.visualizer.splitter.add_destination("passthrough", self.visualizer.plots.get_plot_input(plot_id))

    def build_graph(self):
        # Initialize SAMNA, ensure endpoints correspond to the visualizer
        samna_node = samna.init_samna()
        time.sleep(0.1)

        # Specify visualizer id and open the visualizer window
        visualizer_id = 3
        self.open_visualizer(0.75, 0.75, samna_node.get_receiver_endpoint(), samna_node.get_sender_endpoint(), visualizer_id)
        
        # Open the device, initialize buffer and build SAMNA route
        self.open_device()
        self.init_buf()
        self.build_samna_route()

        # Add an activity plot to the visualizer and set its layout
        activity_plot_id = self.visualizer.plots.add_activity_plot(self.width, self.height, "DVS Layer")
        plot_name = "plot_" + str(activity_plot_id)
        plot = getattr(self.visualizer, plot_name)
        plot.set_layout(0, 0, 0.6, 1)   # set the position: top left x, top left y, bottom right x, bottom right y

        # Build SAMNAGUI event route
        self.build_samnagui_event_route(activity_plot_id)

        # Start DVS graph
        self.dvs_graph.start()

    def open_visualizer(self, window_width, window_height, receiver_endpoint, sender_endpoint, visualizer_id):
        # Start the visualizer in an isolated process, not a sub process
        # The function will not return until the remote node is opened. 
        # Return the opened visualizer.
        gui_cmd = '''%s -c "import samna, samnagui; samnagui.runVisualizer(%f, %f, '%s', '%s', %d)"''' % \
            (sys.executable, window_width, window_height, receiver_endpoint, sender_endpoint, visualizer_id)
        print("Visualizer start command: ", gui_cmd)
        gui_thread = Thread(target=os.system, args=(gui_cmd,))
        gui_thread.start()

        # Wait for the visualizer to open and connect to it
        timeout = 10
        begin = time.time()
        name = "visualizer" + str(visualizer_id)
        while time.time() - begin < timeout:
            try:
                time.sleep(0.1)
                samna.open_remote_node(visualizer_id, name)
            except:
                continue
            else:
                self.visualizer = getattr(samna, name)
                self.gui_thread = gui_thread
                return

        raise Exception("open_remote_node failed:  visualizer id %d can't be opened in %d seconds!!" % (visualizer_id, timeout))

    def start_dvs_show_and_to_buf(self):
        # Build the DVS graph, apply device, and return required variables
        self.build_graph()
        self.apply_device()
        return self.dk, self.visualizer, self.buf, self.gui_thread, self.dvs_graph

class Speck2f(Device):
    def __init__(self, name='Speck2fCharacterizationBoard', width=128, height=128, dvs_converter_name='Speck2fDvsToVizConverter'):
        # Call the constructor of the parent class (Device) to initialize the instance variables
        super(Speck2f, self).__init__(name, width, height, dvs_converter_name)
    
    def init_buf(self):
        # Create a new buffer for storing event data
        self.buf = samna.BasicSinkNode_speck2f_event_output_event()
    
    def apply_device(self):
        # Modify the configuration of the Speck2f device
        config = samna.speck2f.configuration.SpeckConfiguration()
        # Enable DVS event monitoring in the configuration
        config.dvs_layer.monitor_enable = True
        # Apply the new configuration to the device
        self.dk.get_model().apply_configuration(config)

    
class Speck2e(Device):
    def __init__(self, name='Speck2eDevKit', width=128, height=128, dvs_converter_name='Speck2eDvsToVizConverter'):
        # Call the constructor of the parent class (Device) to initialize the instance variables
        super(Speck2e, self).__init__(name, width, height, dvs_converter_name)
    
    def init_buf(self):
        # Create a new buffer for storing event data
        self.buf = samna.BasicSinkNode_speck2e_event_output_event()
    
    def apply_device(self):
        # Modify the configuration of the Speck2e device
        config = samna.speck2e.configuration.SpeckConfiguration()
        # Enable DVS event monitoring in the configuration
        config.dvs_layer.monitor_enable = True
        # Apply the new configuration to the device
        self.dk.get_model().apply_configuration(config)

class Speck2et(Device):
    def __init__(self, name='Speck2etTestBoard', width=128, height=128, dvs_converter_name='Speck2eDvsToVizConverter'):
        # Call the parent constructor with the specified parameters
        super(Speck2et, self).__init__(name, width, height, dvs_converter_name)
    
    def init_buf(self):
        # Initialize the buffer with a BasicSinkNode_speck2e_event_output_event instance
        self.buf = samna.BasicSinkNode_speck2e_event_output_event()

    def apply_device(self):
        # Modify the device configuration
        config = samna.speck2e.configuration.SpeckConfigurationT()
        # Enable DVS event monitoring
        config.dvs_layer.monitor_enable = True
        self.dk.get_model().apply_configuration(config)

class Speck2d(Device):
    def __init__(self, name='Speck2dMiniTestBoard', width=64, height=64, dvs_converter_name='Speck2dMiniDvsToVizConverter'):
        # Call the parent constructor with the specified parameters
        super(Speck2d, self).__init__(name, width, height, dvs_converter_name)
    
    def init_buf(self):
        # Initialize the buffer with a BasicSinkNode_speck2d_mini_event_output_event instance
        self.buf = samna.BasicSinkNode_speck2d_mini_event_output_event()
    
    def apply_device(self):
        # Modify the device configuration
        config = samna.speck2dMini.configuration.SpeckConfiguration()
        # Enable DVS event monitoring
        config.dvs_layer.monitor_enable = True
        self.dk.get_model().apply_configuration(config)

class Speck2c(Device):
    def __init__(self, name='Speck2cMiniTestBoard', width=64, height=64, dvs_converter_name='Speck2cMiniDvsToVizConverter'):
        # Call the parent constructor with the specified parameters
        super(Speck2c, self).__init__(name, width, height, dvs_converter_name)
    
    def init_buf(self):
        # Initialize the buffer with a BasicSinkNode_speck2c_mini_event_output_event instance
        self.buf = samna.BasicSinkNode_speck2c_mini_event_output_event()
    
    def apply_device(self):
        # Modify the device configuration
        config = samna.speck2cMini.configuration.SpeckConfiguration()
        # Enable DVS event monitoring
        config.dvs_layer.monitor_enable = True
        self.dk.get_model().apply_configuration(config)

class Speck2b(Device):
    def __init__(self, name='Speck2bTestBoard', width=128, height=128, dvs_converter_name='Speck2bDvsToVizConverter'):
        # Call the parent constructor with the specified parameters
        super(Speck2b, self).__init__(name, width, height, dvs_converter_name)
    
    def init_buf(self):
        # Initialize the buffer with a BasicSinkNode_speck2b_event_output_event instance
        self.buf = samna.BasicSinkNode_speck2b_event_output_event()
    
    def apply_device(self):
        # Modify the device configuration
        config = samna.speck2b.configuration.SpeckConfiguration()
        # Enable DVS event monitoring
        config.dvs_layer.monitor_enable = True
        self.dk.get_model().apply_configuration(config)

class DVXplorer(Device):
    def __init__(self, name='DVXplorer', width=320, height=240, dvs_converter_name='CameraToVizConverter'):
        # Initialize the DVXplorer device with default parameters or provided ones
        super(DVXplorer, self).__init__(name, width, height, dvs_converter_name)
    
    def init_buf(self):
        # Initialize a buffer for event data coming from the device
        self.buf = samna.BasicSinkNode_camera_event_dvs_event()
    
    def apply_device(self):
        # Start the DVXplorer device to begin sending data
        self.dk.start()

class DeviceFactory:
    """A factory for creating devices"""

    @staticmethod
    def create_device(name):
        """Create a device based on the given name"""

        if name == "Speck2fCharacterizationBoard":
            return Speck2f()
        elif name == "Speck2fModuleDevKit":
            return Speck2f(name="Speck2fModuleDevKit")
        elif name == "Speck2eDevKit":
            return Speck2e()
        elif name == "Speck2eTestBoard":
            return Speck2e(name='Speck2eTestBoard')
        elif name == "Speck2etTestBoard":
            return Speck2et(name='Speck2etTestBoard')
        elif name == "Speck2dMiniTestBoard":
            return Speck2d()
        elif name == "Speck2cMiniTestBoard":
            return Speck2c()
        elif name == "Speck2bTestboard":
            return Speck2b()
        elif name == "Speck2bDevKitTiny":
            return Speck2b(name="Speck2bDevKitTiny")
        elif name == "DVXplorer":
            return DVXplorer()
        else:
            raise ValueError("Unsupported device name: " + name)
    
    @staticmethod
    def get_supported_devices():
        """Return a list of supported device names"""
        supported_devices = ['Speck2fCharacterizationBoard',
                            'Speck2fModuleDevKit'
                            'Speck2eDevKit',
                            'Speck2eTestBoard',
                            'Speck2etTestBoard',
                            'Speck2dMiniTestBoard',
                            'Speck2cMiniTestBoard',
                            'Speck2bTestboard',
                            'Speck2bDevKitTiny',
                            'DVXplorer']
        # print the supported devices
        for device in supported_devices:
            print(device)
        return supported_devices
