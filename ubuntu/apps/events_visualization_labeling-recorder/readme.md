# DVS recorder

## Intro
This tool is used to record data of DVS devices. 

## How to install

````
pip install -r requirements.txt
````

## How to run

````
python run.py
````

## How to use

**1. Start to record**
Click the Record menu and click record, then choose a device(if there are many devices)

**2. Set storage path and filename**
![](./img/set_path_filename.png) 

**3. Start and stop recording**
![](./img/recording.png)

**4. Set Timing(optional)**
If set Timing, recording will automatically stop when the time reaches the timing.
![](./img/set_timing.png)

**5. Set CountDown(optional)**
If set CountDown, recording will start in CountDown seconds.
![](./img/set_countdown.png)

###
**Support devices:** 
- Speck2fCharacterizationBoard
- Speck2eDevKit
- Speck2eTestBoard
- Speck2etTestBoard
- Speck2dMiniTestBoard
- Speck2cMiniTestBoard
- Speck2bTestboard
- Speck2bDevKitTiny
- DVXplorer